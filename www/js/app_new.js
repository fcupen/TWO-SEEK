(function(){

var app = angular.module('ionic.googleMapsEjemplo', ['ionic', 'firebase'])
 .factory('Items', ['$firebaseArray', function($firebaseArray){
    var itemsRef = new Firebase('https://groceries918.firebaseio.com/favoritos');
    return $firebaseArray(itemsRef);
  }])
var fb = null;
	app.run(function($ionicPlatform) {
		$ionicPlatform.ready(function() {
			if(window.cordova && window.cordova.plugins.Keyboard) {
				// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
				// for form inputs)
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

				// Don't remove this line unless you know what you are doing. It stops the viewport
				// from snapping when text inputs are focused. Ionic handles this internally for
				// a much nicer keyboard experience.
				cordova.plugins.Keyboard.disableScroll(true);
			}
			if(window.StatusBar) {
				StatusBar.styleDefault();
			}
			fb = new Firebase("https://groceries918.firebaseio.com/");
		});
	});

app.controller('MapaCtrl', function($rootScope, $scope,$location, $ionicActionSheet,$interval, $ionicSideMenuDelegate, $http,$compile, $state, $ionicListDelegate, Items, $firebaseArray) {
	   
if($location.path() == '/map'){
	   var refh = new Firebase("https://groceries918.firebaseio.com/");
	    var refh2 = new Firebase("https://groceries918.firebaseio.com/posicion/");
	  
	   email = refh.getAuth().password.email;
	   $scope.asdqwe = email;
	   console.log("Authenticated with email:", $scope.asdqwe );
    refh.onAuth(function(authData) {
      if (authData) {
        console.log("Authenticated with uid:", authData.uid);
		console.log("Authenticated with uid:", authData);
      } else {
        console.log("Client unauthenticated.")
      }
    });
	
	$scope.asdf =true;$scope.email = refh.getAuth().password.email;}
 console.log($location.path()); 
    
   $scope.email = $rootScope.email;
	
	
	//menu +
	
	$scope.showActionsheet = function() {
	
		$ionicActionSheet.show({
buttons: [
       { text: 'Complete' }
     ],
	 titleText: 'Acceso Rápido',
     destructiveText: 'Delete',
     
     cancelText: 'Cancel',
     buttonClicked: function(index) {
       return true;
     }
		});
		
	}
	
	
	//menu izq
	$scope.toggleLeft = function(){
		$ionicSideMenuDelegate.toggleLeft()
	}
	//fin menu izq
	//menu der
	$scope.toggleRight = function(){
		$ionicSideMenuDelegate.toggleRight()
	} 
	//fin menu der
var geocoder = new google.maps.Geocoder();

	var directionsDisplay = null;
	var directionsService = null;
	var dire = '';
	
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
	  dire = responses[0].formatted_address;
    } else {
      updateMarkerAddress('1No se puede determinar la dirección en este punto!');
	  dire =  '11No se puede determinar la dirección en este punto!';
    }
  }); 
}
function geocodePosition2(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress2(responses[0].formatted_address);
    } else {
      updateMarkerAddress2('2No se puede determinar la dirección en este punto!');
    }
  });
}
function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}
var latitudes='';



function updateMarkerPosition(latLng) {
	
  document.getElementById('info').innerHTML = [
    latLng.lat(), 
    latLng.lng()
  ].join(', ');
  var gl = new google.maps.LatLng(latLng.lat(),  latLng.lng());
	geocoder.geocode({
    latLng: gl
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
	  dire = responses[0].formatted_address;
    
  
  document.getElementById('coor').innerHTML = latLng.lat();
  document.getElementById('coor2').innerHTML = latLng.lng();
// geocodePosition(gl);
  
$scope.city = {
		id: new Date().getTime().toString(),
		city: dire,
		coor:latLng.lat(), 
		coor2:latLng.lng()
		
	};
	} else {
      updateMarkerAddress('3No se puede determinar la dirección en este punto!');
	  dire =  '33No se puede determinar la dirección en este punto!';
    }
  });
	
} 
function updateMarkerPosition2(latLng) {
	
  document.getElementById('info').innerHTML = [
    latLng.lat(), 
    latLng.lng()
  ].join(', ');
  var gl = new google.maps.LatLng(latLng.lat(),  latLng.lng());
	geocoder.geocode({
    latLng: gl
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress2(responses[0].formatted_address);
	  dire = responses[0].formatted_address;
    
  
  document.getElementById('coor').innerHTML = latLng.lat();
  document.getElementById('coor2').innerHTML = latLng.lng();
// geocodePosition(gl);
  
$scope.city = {
		id: new Date().getTime().toString(),
		city: dire,
		coor:latLng.lat(), 
		coor2:latLng.lng()
		
	};
	} else {
      updateMarkerAddress2('4No se puede determinar la dirección en este punto!');
	  dire =  '44No se puede determinar la dirección en este punto!';
    }
  });
	
} 
//*****************************************************************************Firebase

           $scope.items = Items; 
        
   var algo = new Date().getTime().toString()
	$scope.save = function(){
		$scope.markers=[];
		$scope.items.$add(
         $scope.city
      );
		crearFavorito($scope.city);
		//$state.go('edit',{url:'/edit/2:'+algo} );
		locals();
		//$scope.digest();
		//$scope.apply();
	}
//*****************************************************************************Firebase
function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}	
function updateMarkerAddress2(str) {
  document.getElementById('address2').innerHTML = str;
}		
	var directionsDisplay;

  function iniciarMapa() {
	  
	  
	  var mapOptions = {
        center: new google.maps.LatLng(40.4170339,-3.683689),
        zoom: 14, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDoubleClickZoom: true
    }

    $scope.mapa = new google.maps.Map(document.getElementById('mapa'), mapOptions);

	
    //iniciaremos el mapa en el estanque del parque de El Retiro
    
 
    // intentamos obtener la posicion actual del dispositivo
    navigator.geolocation.getCurrentPosition(
      onSuccess_HighAccuracy,
      onError_HighAccuracy,
       {
        maximumAge:0,
        timeout:100000,
        enableHighAccuracy: false //true
      }
    );
     
    function onSuccess_HighAccuracy(pos) { // dispositivo con GPS
      // centramos el mapa sobre la posicion devuelta por el dispositivo
      $scope.mapa.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      //creamos un marcador en esa posición

var lat = pos.coords.latitude;
var lon = pos.coords.longitude;
var lat2 = pos.coords.latitude+0.0033;
var lon2 = pos.coords.longitude+0.0033;

var glatlon = new google.maps.LatLng(lat, lon);
var glatlon2 = new google.maps.LatLng(lat2, lon2);
var lalo1 = new google.maps.LatLng(10.196355543476686,-68.00566746917724);

//**

  var latLng = new google.maps.LatLng(lat, lon);//(10.232197476178218, -67.88286569814761);
  
      var initialLocation = new google.maps.LatLng(lat,lon);
      $scope.center = function(){$scope.mapa.setCenter(initialLocation)};
    
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'A dónde quiero ir!',
    map: $scope.mapa,
    draggable: false
  });

	var marker3 = new google.maps.Marker({
    position: glatlon2,
    title: 'A dónde quiero ir!',
    map: $scope.mapa,
    draggable: true,
	icon: 'img/inicial.png'
  });

  //ICONOS
  //fav1.setIcon('img/fav.png');
  
  //marker.setIcon('img/hand.png');
  marker3.setIcon('img/hand.png');
  
  google.maps.event.addListener(marker3, 'dragend', function(){ 
		openInfoWindow(marker3); 
		closeInf(marker3);
		updateMarkerPosition2(marker3.getPosition());
		geocodePosition2(marker3.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker3.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap($scope.mapa);
				
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	google.maps.event.addListener(marker3, 'dblclick', function(){ 
		openInfoWindow(marker3); 
		closeInf(marker3);
		updateMarkerPosition2(marker3.getPosition());
		geocodePosition2(marker3.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker3.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap($scope.mapa);
				
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
  
  google.maps.event.addListener(marker3, 'click', function(){ 
		openInf(marker3); 
		//infoWindow3.close(marker2);
		closeInfoWindow(marker3);
		geocodePosition2(marker3.getPosition());
		updateMarkerPosition2(marker3.getPosition());
		
		
	});
  
  //FIN ICONOS
//***
var obj={content:'<h4>Estoy Aquí!</h4><strong>Dirección:</strong><div id="address2"></div>'}
infoWindow3 = new google.maps.InfoWindow(obj);
var obj2={content:'<strong>Estoy Aquí!</strong> <br /> Haz doble Click.'}
infoWindow4 = new google.maps.InfoWindow(obj2);


//MI POSICION

//---
var compiled = $compile('<div><h4>A donde quiero Ir!</h4>Dirección:<div id="address2"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><br/> <br/>Arrástrame para elegir el Lugar.<br /> <div class="button-bar" ><button ng-click="" class="button icon-left ion-android-car button-balanced">Ir aquí!</button><a class="button icon-left ion-star button-positive" ng-click="save()">Favorito</a></div><br/></div>')($scope);


function openInfoWindow(marker) {
	    var markerLatLng = marker.getPosition();

	    infoWindow.open($scope.mapa, marker);
	}
	infoWindow = new google.maps.InfoWindow({ content: compiled[0]});
	
	var compiled2 = $compile('<div><h4>Favorito</h4><strong>Dirección:</strong><div id="address"></div><br />Haz doble click Aquí!<div class="button-bar"><a  href="#/edit"class="button ion-android-car button-balanced"></a></div><br/></div>')($scope);

	function openI(marker) {
	    var markerLatLng = marker.getPosition();

	    infoW.open($scope.mapa, marker);
	}
	infoW = new google.maps.InfoWindow({ content: compiled2[0]});
	
	var compiled3 = $compile('<div><h4>Favorito!</h4><strong>Dirección:</strong><div id="address"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><br/> <br/> <div class="button-bar"><a  href="#/edit" class="button icon-left ion-android-car button-balanced">Ir aquí!</a></div><br/></div>')($scope);

	function openIn(marker) {
	    var markerLatLng = marker.getPosition();

	    infoWn.open($scope.mapa, marker);
	}
	infoWn = new google.maps.InfoWindow({ content: compiled3[0]});
	
	
	var compiled4 = $compile('<div><h4>A dónde quiero ir!</h4><strong>Dirección:</strong><div id="address2"></div><br/>Arrástrame para elegir el Lugar.<br/><div class="button-bar"><a  href="#/edit" class="button ion-android-car button-balanced"></a> <a  ng-click="save()" class="button ion-star button-positive"></a></div></div>')($scope);

	function openInf(marker) {
	    var markerLatLng = marker.getPosition();

	    infoWnf.open($scope.mapa, marker);
	}
	infoWnf = new google.maps.InfoWindow({ content: compiled4[0]});
	
	function closeInfoWindow(marker) {infoWindow.close($scope.mapa, marker);}
	function closeI(marker) {infoW.close($scope.mapa, marker);}
	function closeIn(marker) {infoWn.close($scope.mapa, marker);}
	function closeInf(marker) {infoWnf.close($scope.mapa, marker);}
	//----
	

	
	$scope.markers = [];
	$scope.markers2 = [];
	//------------------------****************************************************
	//------------------------****************************************************
	//------------------------****************************************************
	
           
        
var createMarker = function ( city, coor,coor2){
	
       
        var marker = new google.maps.Marker({
            map: $scope.mapa,
            position: new google.maps.LatLng(coor, coor2),
            title: city,
			icon:'img/fav.png',
			center: new google.maps.LatLng(coor, coor2)
        });

	// fin FUNCIONES FAVORITOS--------------------
		google.maps.event.addListener(marker, 'click', function(){ 		
		closeInfoWindow(marker3);
		closeInf(marker3);
		openI(marker); 
		closeIn(marker);
		rutas2(marker);
		updateMarkerPosition(marker.getPosition());
	});
	google.maps.event.addListener(marker, 'dblclick', function(){ 
		openIn(marker);
		closeInfoWindow(marker3);
		closeInf(marker3);
		closeI(marker); 
		rutas2(marker);
	});

			$scope.markers.push(marker);

    }    
   
	ciudad = Items;
	
	var createMarker2 = function ( city, coor,coor2){
	
       
        var marker = new google.maps.Marker({
            map: $scope.mapa,
            position: new google.maps.LatLng(coor, coor2),
            title: city,
			icon:'img/location.png',
			center: new google.maps.LatLng(coor, coor2)
        });

        // FUNCIONES FAVORITOS--------------------------
	
	// fin FUNCIONES FAVORITOS--------------------
		google.maps.event.addListener(marker, 'click', function(){ 	
		closeInfoWindow(marker3);
		closeInf(marker3);
		openI(marker); 
		closeIn(marker);
		rutas2(marker);
		updateMarkerPosition(marker.getPosition());
	});
	google.maps.event.addListener(marker, 'dblclick', function(){ 
		openIn(marker);
		closeInfoWindow(marker3);
		closeInf(marker3);
		closeI(marker); 
		rutas2(marker);
	});
			$scope.posi.push(marker);
		console.log('ADD: '+marker);
    }    
	//END
	//**************************************************************
	    var ref = new Firebase("https://groceries918.firebaseio.com/favoritos");
		var ref2 = new Firebase("https://groceries918.firebaseio.com/");
		var sp = $firebaseArray(ref2.child("favoritos/"));
		var sp2 = $firebaseArray(ref2.child("posicion/"));
		$scope.posicion = sp2;
		
var keyy = ref2.child('Autopista caracas-Valencia, Guacara, Venezuela');
var fredFirstNameRef = ref2.child('city');
var path = fredFirstNameRef.toString();
console.log(keyy);
console.log(sp);
console.log(path);

//*******************----------------------
var ref1 = new Firebase("https://docs-examples.firebaseio.com/web/saving-data/fireblog/posts");
// Retrieve new posts as they are added to our database

//-**-*-*-*-*-*--*-
    ref.on("value", function(allMessagesSnapshot) {
		console.log('VALUE');$scope.markers=[];
      allMessagesSnapshot.forEach(function(messageSnapshot) {
        
        var key = messageSnapshot.key(); 
        var city = messageSnapshot.child("city").val();  
        var coor = messageSnapshot.child("coor").val();  
		var coor2 = messageSnapshot.child("coor2").val();
		
		for (i = 0; i < 1; i++){ 
        createMarker(city,coor,coor2);
		
		}

      });
    });
	refh2.on("child_added", function(allMessagesSnapshot) {
		console.log('VALUE POSICION');$scope.posi=[];
      //allMessagesSnapshot.forEach(function(messageSnapshot) {
        
		var nombre_a;
	var aux_a;
	var nombre_a = [];
	
	if(nombre.length <='0'){
		nombre.push(nombre_a);
	}else{
	for (i = 0; i < nombre.length; i++){
		if(nombre[i] == nombre_a){
			aux = 1 + aux;
		}else{aux = aux;}
	}
	if (aux == '0'){
		nombre.push(nombre_a);
	}
	}
		
		
		
        var key = allMessagesSnapshot.key(); 
        var city = allMessagesSnapshot.child("user").val();  
        var coor = allMessagesSnapshot.child("lat").val();  
		var coor2 = allMessagesSnapshot.child("lon").val();
		
		for (i = 0; i < 1; i++){ 
        createMarker2(city,coor,coor2);
		 
	  }
	  //});
	  }
	  );
	
	 $scope.openInfoWindow2 = function(e, selectedMarker){
        e.preventDefault();
        google.maps.event.trigger(selectedMarker, 'click');
    }
	 
	
	
	
	google.maps.event.addListener($scope.mapa, 'dblclick', function(event) {
//setMapOnAll(null);

marker3.setPosition(event.latLng); 
//marker.setVisible(false);
openInfoWindow(marker3); 
		closeInf(marker3);
		updateMarkerPosition2(marker3.getPosition());
		geocodePosition2(marker3.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker3.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap($scope.mapa);
				
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
   
});
	
	//FIN DIBUJAR LA RUTA AL SOLTAR EL MARKER
	//FIN ARRASTRAR
	
	function rutas(marker){
		geocodePosition(marker.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay2.setMap($scope.mapa);
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay2.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
	}
	
	
	function rutas2(marker){
		geocodePosition(marker.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay2.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
	}
	//CLICK
	
	//FIN CLICK
	
//*****
	var ook={
	map:$scope.mapa,
	suppressMarkers:true,polylineOptions: {
      strokeColor: "red"
    }
	}
	var ook2={
	map:$scope.mapa,
	suppressMarkers:true,polylineOptions: {
      strokeColor: "green"
    }
	}
	directionsDisplay = new google.maps.DirectionsRenderer(ook);
	
	directionsDisplay2 = new google.maps.DirectionsRenderer(ook2);
		directionsService = new google.maps.DirectionsService();
	
	
	
//***
 

 //------------******************************
 //-- END FN_OK 

	   $(document).ready(function(){
        var miMarcador;
         
        var miGeocoder = new google.maps.Geocoder();
 
        $("#direccion").autocomplete({
      source: function(request, response){ // comportamiento al escribir la direccion
         var term = request.term; // dirección de búsqueda
         miGeocoder.geocode({'address': term}, // se la pasamos al objeto Geocoder
         function(results, status){// función controladora de los resutados desde Google
            var resultado = [];
            if (status == google.maps.GeocoderStatus.OK) {// se han encontrado resultados
               objDireccion = new Object();
               $.each(results, function(index, value){// los añadimos a un array
                  objDireccion = value;
                  objDireccion.value = value.formatted_address;
                  resultado.push(objDireccion);
               });
            }
            response(resultado);
         });
      },
      delay: 100,
      minLength: 2,
      select: function(event,ui) {  // comportamiento al seleccionar la direccion que sugiere Google                       
         if(ui.item){
            actualizarCampos(ui.item); // función de refresco del interfaz
            // creamos una nueva posición con los datos recibidos
            var posicion = new google.maps.LatLng(
               ui.item.geometry.location.lat(), 
               ui.item.geometry.location.lng()
            );
            //centramos el mapa en la posicion, ajustamos el zoom y creamos un marcador
            $scope.mapa.setCenter(posicion);
            $scope.mapa.setZoom(14);
            // si el marcador ya existe lo eliminamos
            if(miMarcador!=undefined){miMarcador.setMap(null);}            
            miMarcador = new google.maps.Marker({
               map: $scope.mapa, 
               position: posicion, 
               draggable: true // para que se pueda arrastrar con el ratón
            });
			miMarcador.setIcon('img/hand.png');
			updateMarkerPosition(miMarcador.getPosition());
			
			
		 google.maps.event.addListener(miMarcador, 'click', function(){ 
		openInf(miMarcador); 
		closeInfoWindow(miMarcador);
		geocodePosition2(miMarcador.getPosition());
		updateMarkerPosition(miMarcador.getPosition());
		
		
	});
		 google.maps.event.addListener(miMarcador, 'dragend', function(){ 
		openInfoWindow(miMarcador); 
		closeInf(miMarcador);
		rutas(miMarcador);updateMarkerPosition(miMarcador.getPosition());
		geocodePosition2(miMarcador.getPosition());
		var request = {
		        origin: glatlon,
		        destination: miMarcador.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap($scope.mapa);
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	
		 google.maps.event.addListener(miMarcador, 'dblclick', function(){ 
		openInfoWindow(miMarcador); 
		closeInf(miMarcador);
		rutas(miMarcador);updateMarkerPosition(miMarcador.getPosition());
		geocodePosition(miMarcador.getPosition());
		var request = {
		        origin: glatlon,
		        destination: miMarcador.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap($scope.mapa);
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	
		 
         }   
      }
   });
    });
     $scope.cities = cities;
    function actualizarCampos(objDireccion){
        $('#latitud').val(objDireccion.geometry.location.lat());
        $('#longitud').val(objDireccion.geometry.location.lng());
        $('#direccion').val(objDireccion.formatted_address);
    }
 
 
 
 
 
	//----- GEOLOCALIZAR
	
	var watchProcess = null;
	var inof;
	
	function pos_ini() {
		
          if (watchProcess == null) {
            

            function locError(error) {
                // the current position could not be located
                alert("The current position could not be found!");
            }

            function setCurrentPosition21(pos) {
			console.log( pos.coords.latitude);
			console.log(pos.coords.longitude);
                currentPositionMarker = new google.maps.Marker({
                    map: $scope.mapa,
                    position: new google.maps.LatLng(
                        pos.coords.latitude,
                        pos.coords.longitude
                    ),
                    title: "Current Position"
                });
                updateMarkerPosition( new google.maps.LatLng(
                        pos.coords.latitude,
                        pos.coords.longitude
                    ));
            }
            function displayAndWatch(position) {
                // set current position
				
                setCurrentPosition21(position);
                // watch position
                watchCurrentPosition();
            }

			function fnok(position) {
			console.log('asd');
                        setMarkerPosition(
                            currentPositionMarker,
                            position
                        );
                    }
			function fnerr(position) {
			console.log('ERROR');
                        
                    }		
					
            function watchCurrentPosition() {
                var positionTimer = navigator.geolocation.watchPosition(fnok,fnerr,{enableHighAccuracy: true,
        maximumAge: 300000,
        timeout: 20000});
            }

            function setMarkerPosition(marker, position) {
                document.getElementById('lat').innerHTML ='Latitud: '+position.coords.latitude+', Longitud: '+position.coords.longitude;
        
				console.log('+'+position.coords.latitude);
				console.log('+'+position.coords.longitude);
				console.log('LAT '+position.coords.latitude);
		$scope.posicion.$add({
			
			'lat': position.coords.latitude,'lon':position.coords.longitude, 'user':$scope.asdqwe
        
      });
				marker.setPosition(
				
                    new google.maps.LatLng(
                        position.coords.latitude,
                        position.coords.longitude)
                );
            }

            function initLocationProcedure() {
                
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(displayAndWatch, locError,{
        maximumAge: 3000,
        timeout: 20000,
        enableHighAccuracy: true //true
      });
                } else {
                    alert("Your browser does not support the Geolocation API");
                }
            }
watchProcess=0110;
            $(document).ready(function() {
                initLocationProcedure();
				$scope.inof = $interval(function(){watchCurrentPosition();console.log('1');$scope.$apply;},10000);
	
            });
	}}
	
	function pos_can() { $interval.cancel($scope.inof);}
	
	
			jQuery(window).ready(function(){
            
            jQuery("#watchPositionBtn").click(pos_ini);
            jQuery("#stopWatchBtn").click(pos_can);
        });
			
		
	//----- GEOLOCALIZAR
 
 
 
 
 
 
	  
    }
 
    function onError_HighAccuracy(error) {// dispositivo sin GPS
      if (error.code == error.TIMEOUT){ // si no ha dado tiempo volvemos a intentarlo
        navigator.geolocation.getCurrentPosition(
          onSuccess_HighAccuracy,
          onError_LowAccuracy, // si ha dado error desistimos
          {
             maximumAge: 30000, timeout: 27000,
            enableHighAccuracy: false
          }
        );
      }
      else{// mostramos una alerta con el error
        alert(JSON.stringify(error)); 
      }
    }
 
    function onError_LowAccuracy(error) { // mostramos una alerta con el error
      alert(JSON.stringify(error)); 
    }
    
	
  }
 
  // añadimos el evento al mapa para iniciar el proceso
  google.maps.event.addDomListener(window, 'load', iniciarMapa);
 // fin e inicio de b_dir
 

   
    
   
   
   
   
 
 
 
 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
 
});



function locals(){
			window.localStorage['cities'] = angular.toJson(cities);
		
		
		}
var cities = angular.fromJson(window.localStorage['cities'] || '[]');


function crearFavorito(city){
		cities.push(city);
}
	/*
app.controller('ListCtrl', function($scope){
	
	$scope.cities = cities;
	
});
*/
app.controller('AddCtrl', function($scope, $state){
	
	$scope.city = {
		id: new Date().getTime().toString(),
		city: 'Fav',
		coor: '',
		coor2:''
		
	};
	
	$scope.save = function(){
		crearFavorito($scope.city);
		$state.go('index'); 
		locals();
	}
});

function editarFavorito(city){
	for (var i = 0; i< cities.length; i++){
		if(cities[i].id === city.id){
			cities[i] = city;
			return;
		}
	}
}

function obtenerFavorito(cityId){
	
	for (var i = 0; i< cities.length; i++){
		if(cities[i].id === cityId){
			return cities[i];
		}
	}return undefined;
	
}

app.controller("LoginController", function($rootScope, $scope, $firebaseAuth, $location) {
 $scope.asdf = true;
 if($location.path() == '/login'){$scope.asdf =false;}
 console.log($location.path());
 
  
  
    $scope.login = function(username, password) {
		
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function(authData) {
            $location.path("/map");console.log(username);$rootScope.email = username;
        }).catch(function(error) {
            console.error("ERROR: " + error);
        });
    }
 
    $scope.register = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$createUser({email: username, password: password}).then(function() {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function(authData) {
            $location.path("/map");
        }).catch(function(error) {
            console.error("ERROR " + error);
        });
    }
 
});


app.controller('EditCtrl', function($scope, $state){
	
	//%scope.city = angular.copy(obtenerFavorito($state.params.cityId));
	
	$scope.save = function(){
		editarFavorito($scope.city);
		$state.go('index');
		locals(); 
	};
	
}); 

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('index',{
		url:'/map',
		templateUrl:'tpl/index.html'
	});
	$stateProvider.state('list',{
		url:'/list',
		templateUrl:'tpl/list.html'
	});
	$stateProvider.state('edit',{
		url:'/edit/:cityId',
		templateUrl:'tpl/edit.html',
		 controller: 'EditCtrl'
	});
	$stateProvider.state('login',{ 
		url:'/login',
		templateUrl:'tpl/login.html',
		controller: 'LoginController'
	});
	$stateProvider.state('add',{ 
		url:'/add',
		templateUrl:'tpl/edit.html',
		controller: 'AddCtrl'
	});
	$stateProvider.state('mip',{ 
		url:'/mip',
		templateUrl:'tpl/mip.html',
		controller: 'mip'
	});
	$urlRouterProvider.otherwise('/login');
});





}());











