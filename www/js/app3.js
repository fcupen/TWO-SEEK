(function(){

var app = angular.module('ionic.googleMapsEjemplo', ['ionic'])
 
app.controller('MapaCtrl', function($scope, $ionicSideMenuDelegate, $http,$compile, $state) {
	$scope.toggleLeft = function(){
		$ionicSideMenuDelegate.toggleLeft()
	}
	
	$scope.toggleRight = function(){
		$ionicSideMenuDelegate.toggleRight()
	}

var geocoder = new google.maps.Geocoder();

	var directionsDisplay = null;
	var directionsService = null;
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}
function geocodePosition2(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress2(responses[0].formatted_address);
    } else {
      updateMarkerAddress2('Cannot determine address at this location.');
    }
  });
}
function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
}
function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}	
function updateMarkerAddress2(str) {
  document.getElementById('address2').innerHTML = str;
}		
	var directionsDisplay;

  function iniciarMapa() {
	  
	  
	  
    //iniciaremos el mapa en el estanque del parque de El Retiro
    var mapa = new google.maps.Map(
      document.getElementById("mapa"), 
      { 
        center: new google.maps.LatLng(40.4170339,-3.683689),
        zoom: 14, 
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );
 
    // intentamos obtener la posicion actual del dispositivo
    navigator.geolocation.getCurrentPosition(
      onSuccess_HighAccuracy,
      onError_HighAccuracy,
       {
        maximumAge:0,
        timeout:100000,
        enableHighAccuracy: false //true
      }
    );
     
    function onSuccess_HighAccuracy(pos) { // dispositivo con GPS
      // centramos el mapa sobre la posicion devuelta por el dispositivo
      mapa.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      //creamos un marcador en esa posición
      /*var myPosicion = new google.maps.Marker(
        {
          position: new google.maps.LatLng(10.232197476178218, -67.88286569814761),
          map: mapa,draggable: true
        }
      );
	  */
var lat = pos.coords.latitude;
var lon = pos.coords.longitude;
var lat2 = pos.coords.latitude+0.0033;
var lon2 = pos.coords.longitude+0.0033;

var glatlon = new google.maps.LatLng(lat, lon);
var glatlon2 = new google.maps.LatLng(lat2, lon2);
var lalo1 = new google.maps.LatLng(10.196355543476686,-68.00566746917724);
var lalo2 = new google.maps.LatLng(10.227506557144835,-67.87729967456056);
var lalo3 = new google.maps.LatLng(10.213526980949078,-67.89601076464851);
var lalo4 = new google.maps.LatLng(10.235488576963009,-67.96330202441413);
var lalo5 = new google.maps.LatLng(10.190761341119549,-68.00370682101448);
//**

  var latLng = new google.maps.LatLng(lat, lon);//(10.232197476178218, -67.88286569814761);
  
  var marker = new google.maps.Marker({
    position: glatlon2,
    title: 'A dónde quiero ir!',
    map: mapa,
    draggable: true
  });
  var marker2 = new google.maps.Marker({
    position: latLng,
    title: 'Estoy aquí!',
    map: mapa
  });

  var fav1 = new google.maps.Marker({
    position: lalo1,
    title: 'Fav 1',
    map: mapa,
	center: lalo1
  });
  var fav2 = new google.maps.Marker({
    position: lalo2,
    title: 'Fav 2',
    map: mapa,
	center: lalo2
  });
  var fav3 = new google.maps.Marker({
    position: lalo3,
    title: 'Fav 3',
    map: mapa,
	center: lalo3
  });
  var fav4 = new google.maps.Marker({
    position: lalo4,
    title: 'Fav 4',
    map: mapa,
	center: lalo4
  });
  var fav5 = new google.maps.Marker({
    position: lalo5,
    title: 'Fav 5',
    map: mapa,
	center: lalo5
  });
  //ICONOS
  fav1.setIcon('img/fav.png');
  fav2.setIcon('img/fav.png');
  fav3.setIcon('img/fav.png'); 
  fav4.setIcon('img/fav.png');
  fav5.setIcon('img/fav.png');
  marker.setIcon('img/hand.png');
  
  //FIN ICONOS
//***
var obj={content:'<h4>Estoy Aquí!</h4><strong>Dirección:</strong><div id="address2"></div>'}
infoWindow3 = new google.maps.InfoWindow(obj);
var obj2={content:'<strong>Estoy Aquí!</strong> <br /> Haz doble Click.'}
infoWindow4 = new google.maps.InfoWindow(obj2);


//MI POSICION
google.maps.event.addListener(marker2, 'click', function(){ 

infoWindow3.close(mapa, marker2);
var markerLatLng = marker2.getPosition();

		infoWindow4.open(mapa, marker2);
		geocodePosition2(marker2.getPosition());
		updateMarkerPosition(marker2.getPosition());
	});
google.maps.event.addListener(marker2, 'dblclick', function(){ 


var markerLatLng = marker2.getPosition();
infoWindow4.close(mapa, marker2);
		infoWindow3.open(mapa, marker2);
		geocodePosition2(marker2.getPosition());
		updateMarkerPosition(marker2.getPosition());
	});
//FIN MI POSICION
// VERDADERA Distancia
	var rad = function(x) {
  return x * Math.PI / 180;
};

var getDistance = function(p1, p2) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = rad(p2.lat() - p1.lat());
  var dLong = rad(p2.lng() - p1.lng());
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d; // returns the distance in meter
};
	
	// VERDADERA DISTANCIA -> getDistance(markerLatLng,markerLatLng2)

	  
//---		
var compiled = $compile('<div><h4>A donde quiero Ir!</h4>Dirección:<div id="address"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><br/> <br/>Arrástrame para elegir el Lugar.<br /> <div class="button-bar" ><button ng-click="loadData()" class="button icon-left ion-android-car button-balanced">Ir aquí!</button><a class="button icon-left ion-star button-positive">Favorito</a></div><br/></div>')($scope);


function openInfoWindow(marker) {
	    var markerLatLng = marker.getPosition();
		var markerLatLng2 = marker2.getPosition();


	   /* infoWindow.setContent([
	        '<h4>A dónde quiero Ir!</h4><strong>Dirección:</strong><div id="address"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><strong>',
	        markerLatLng.lat(),
	        ', ',
	        markerLatLng.lng(),
	        '</strong><br/> <br/>Arrástrame para elegir el Lugar.<br /> <div class="button-bar"  ng-controller="favorito" ><button ng-click="loadData()" class="button icon-left ion-android-car button-balanced">Ir aquí!</button><a class="button icon-left ion-star button-positive">Favorito</a></div><br/>'
	    ].join(''));*/
	    infoWindow.open(mapa, marker);
	}
	infoWindow = new google.maps.InfoWindow({ content: compiled[0]});
	
	var compiled2 = $compile('<div><h4>Favorito</h4><strong>Dirección:</strong><div id="address"></div><br />Haz doble click Aquí!<div class="button-bar"><a  href="#/edit"class="button ion-android-car button-balanced"></a></div><br/></div>')($scope);

	function openI(marker) {
	    var markerLatLng = marker.getPosition();
		var markerLatLng2 = marker2.getPosition();

	/*
	    infoW.setContent([
	        '<h4>Favorito</h4><strong>Dirección:</strong><div id="address"></div><br />Haz doble click Aquí!<div class="button-bar"><a  href="#/edit"class="button ion-android-car button-balanced"></a></div><br/>'
	    ].join(''));*/
	    infoW.open(mapa, marker);
	}
	infoW = new google.maps.InfoWindow({ content: compiled2[0]});
	
	var compiled3 = $compile('<div><h4>Favorito!</h4><strong>Dirección:</strong><div id="address"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><br/> <br/> <div class="button-bar"><a  href="#/edit" class="button icon-left ion-android-car button-balanced">Ir aquí!</a></div><br/></div>')($scope);

	function openIn(marker) {
	    var markerLatLng = marker.getPosition();
		var markerLatLng2 = marker2.getPosition();

	/*
	    infoWn.setContent([
	        '<h4>Favorito!</h4><strong>Dirección:</strong><div id="address"></div><table><tr><td><strong>Distancia: </strong></td><td><div id="distance"></div></td></tr></table><table><tr><td><strong>Duración: </strong></td><td><div id="duration"></div></td></tr></table><strong>',
	        markerLatLng.lat(),
	        ', ',
	        markerLatLng.lng(),
	        '</strong><br/> <br/> <div class="button-bar"><a  href="#/edit" class="button icon-left ion-android-car button-balanced">Ir aquí!</a></div><br/>'
	    ].join(''));*/
	    infoWn.open(mapa, marker);
	}
	infoWn = new google.maps.InfoWindow({ content: compiled3[0]});
	
	
	var compiled4 = $compile('<div><h4>A dónde quiero ir!</h4><strong>Dirección:</strong><div id="address"></div><br/>Arrástrame para elegir el Lugar.<br/><div class="button-bar"><a  href="#/edit" class="button ion-android-car button-balanced"></a><a class="button ion-star button-positive"></a></div></div>')($scope);

	function openInf(marker) {
	    var markerLatLng = marker.getPosition();
		var markerLatLng2 = marker2.getPosition();

	/*
	    infoWnf.setContent([
	        '<h4>A dónde quiero ir!</h4><strong>Dirección:</strong><div id="address"></div><br/>Arrástrame para elegir el Lugar.<br/><div class="button-bar"><a  href="#/edit" class="button ion-android-car button-balanced"></a><a class="button ion-star button-positive"></a></div>'
	    ].join(''));*/
	    infoWnf.open(mapa, marker);
	}
	infoWnf = new google.maps.InfoWindow({ content: compiled4[0]});
	
	function closeInfoWindow(marker) {infoWindow.close(mapa, marker);}
	function closeI(marker) {infoW.close(mapa, marker);}
	function closeIn(marker) {infoWn.close(mapa, marker);}
	function closeInf(marker) {infoWnf.close(mapa, marker);}
	//----
	
	
	//----
	
	// ARRASTRAR
	/*
	google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Arrastrando...');
	openInfoWindow(marker); 
    updateMarkerPosition(marker.getPosition());
  });*/
	//**DIBUJAR LA RUTA AL SOLTAR EL MARKER
    google.maps.event.addListener(marker, 'dragend', function(){ 
		openInfoWindow(marker); 
		closeInf(marker);
		updateMarkerPosition(marker.getPosition());
		geocodePosition(marker.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap(mapa);
				
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	//FIN DIBUJAR LA RUTA AL SOLTAR EL MARKER
	//FIN ARRASTRAR click
	
	function rutas(marker){
		geocodePosition(marker.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay2.setMap(mapa);
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay2.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
	}
	function rutas2(marker){
		geocodePosition(marker.getPosition());
		var request = {
		        origin: glatlon,
		        destination: marker.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	        document.getElementById('distance').innerHTML =response.routes[0].legs[0].distance.value/1000 + " km";
			document.getElementById('duration').innerHTML =response.routes[0].legs[0].duration.value/60 + " min";
	            directionsDisplay2.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
	}
	//CLICK
	//CLICK AL MARKER PARA DIBUJAR LA RUTA
	//-----------------------------------------****************************************************************
	 google.maps.event.addListener(fav1, 'click', function(){ 
		openI(fav1); 
		closeIn(fav1);
		rutas2(fav1);updateMarkerPosition(fav1.getPosition());
	});
	google.maps.event.addListener(fav2, 'click', function(){ 
		openI(fav2); 
		closeIn(fav2);
		rutas2(fav2);updateMarkerPosition(fav2.getPosition());
	});
	google.maps.event.addListener(fav3, 'click', function(){ 
		openI(fav3); 
		closeIn(fav3);
		rutas2(fav3);updateMarkerPosition(fav3.getPosition());
	});
	google.maps.event.addListener(fav4, 'click', function(){ 
		openI(fav4); 
		closeIn(fav4);
		rutas2(fav4);updateMarkerPosition(fav4.getPosition());
	});
	google.maps.event.addListener(fav5, 'click', function(){ 
		openI(fav5); 
		closeIn(fav5);
		rutas2(fav5);updateMarkerPosition(fav5.getPosition());
	});
	google.maps.event.addListener(fav1, 'dblclick', function(){ 
		openIn(fav1);
		closeI(fav1); 
		rutas(fav1);updateMarkerPosition(fav1.getPosition());
	});
	google.maps.event.addListener(fav2, 'dblclick', function(){ 
		openIn(fav2); 
		closeI(fav2);
		rutas(fav2);updateMarkerPosition(fav2.getPosition());
	});
	google.maps.event.addListener(fav3, 'dblclick', function(){ 
		openIn(fav3); 
		closeI(fav3);
		rutas(fav3);updateMarkerPosition(fav3.getPosition());
	});
	google.maps.event.addListener(fav4, 'dblclick', function(){ 
		openIn(fav4); 
		closeI(fav4);
		rutas(fav4);updateMarkerPosition(fav4.getPosition());
	});
	google.maps.event.addListener(fav5, 'dblclick', function(){ 
		openIn(fav5); 
		closeI(fav5);
		rutas(fav5);updateMarkerPosition(fav5.getPosition());
	});
	
	//FIN CLICK AL MARKER PARA DIBUJAR LA RUTA
	/*
	google.maps.event.addListener(marker2, 'click', function(){ 
		
		openInfoWindow(marker2);
		geocodePosition(marker2.getPosition());
		updateMarkerPosition(marker2.getPosition());
		
		
	});*/
	
	
	
	google.maps.event.addListener(marker, 'click', function(){ 
		openInf(marker); 
		closeInfoWindow(marker);
		geocodePosition(marker.getPosition());
		updateMarkerPosition(marker.getPosition());
		
		
	});
	
	//FIN CLICK
	
//*****
	var ook={
	map:mapa,
	suppressMarkers:true,polylineOptions: {
      strokeColor: "red"
    }
	}
	var ook2={
	map:mapa,
	suppressMarkers:true,polylineOptions: {
      strokeColor: "green"
    }
	}
	directionsDisplay = new google.maps.DirectionsRenderer(ook);
	
	directionsDisplay2 = new google.maps.DirectionsRenderer(ook2);
		directionsService = new google.maps.DirectionsService();
	
	
	
//***
 
 //----------********************
 /*
 var objConfigDR = {
	map:mapa
 }
 var objConfigDS = {
	origin:'avenida piar, guacara',//ORIGEN
	destination:'avenida bolivar norte, valencia',//DESTINO
	travelMode: google.maps.TravelMode.DRIVING
 }
 
 var ds = new google.maps.DirectionsService();
 var dr = new google.maps.DirecttonsRenderer(objConfigDR);
  ds.route(objConfigDS, fnRutear);
  function fnRutear(resultados, status){
  if(status == 'OK'){
  dr.setDirections(resultados);
  }else{alert('Error'+status);}
  
  }*/
 //------------******************************
 //-- END FN_OK 

	   $(document).ready(function(){
        var miMarcador;
         
        var miGeocoder = new google.maps.Geocoder();
 
        $("#direccion").autocomplete({
      source: function(request, response){ // comportamiento al escribir la direccion
         var term = request.term; // dirección de búsqueda
         miGeocoder.geocode({'address': term}, // se la pasamos al objeto Geocoder
         function(results, status){// función controladora de los resutados desde Google
            var resultado = [];
            if (status == google.maps.GeocoderStatus.OK) {// se han encontrado resultados
               objDireccion = new Object();
               $.each(results, function(index, value){// los añadimos a un array
                  objDireccion = value;
                  objDireccion.value = value.formatted_address;
                  resultado.push(objDireccion);
               });
            }
            response(resultado);
         });
      },
      delay: 100,
      minLength: 2,
      select: function(event,ui) {  // comportamiento al seleccionar la direccion que sugiere Google                       
         if(ui.item){
            actualizarCampos(ui.item); // función de refresco del interfaz
            // creamos una nueva posición con los datos recibidos
            var posicion = new google.maps.LatLng(
               ui.item.geometry.location.lat(), 
               ui.item.geometry.location.lng()
            );
            //centramos el mapa en la posicion, ajustamos el zoom y creamos un marcador
            mapa.setCenter(posicion);
            mapa.setZoom(14);
            // si el marcador ya existe lo eliminamos
            if(miMarcador!=undefined){miMarcador.setMap(null);}            
            miMarcador = new google.maps.Marker({
               map: mapa, 
               position: posicion, 
               draggable: true // para que se pueda arrastrar con el ratón
            });
			miMarcador.setIcon('img/hand.png');
			updateMarkerPosition(miMarcador.getPosition());
			
			
            // definimos una función que escuchará el evento de fin de arrastre para volver a 
           /* google.maps.event.addListener(miMarcador, 'dragend', function(evento) { 
               miGeocoder.geocode({'latLng': evento.latLng}, 
               function(results, status){
                  if (status == google.maps.GeocoderStatus.OK) {
                     $('#latitud').val(evento.latLng.lat());
                     $('#longitud').val(evento.latLng.lng());   
                     actualizarCampos(results[0]); 
						rutas(miMarcador);
                  }     
                  }
               )
         });*/
		 google.maps.event.addListener(miMarcador, 'click', function(){ 
		openInf(miMarcador); 
		closeInfoWindow(miMarcador);
		geocodePosition(miMarcador.getPosition());
		updateMarkerPosition(miMarcador.getPosition());
		
		
	});
		 google.maps.event.addListener(miMarcador, 'dragend', function(){ 
		openInfoWindow(miMarcador); 
		closeInf(miMarcador);
		rutas(miMarcador);updateMarkerPosition(miMarcador.getPosition());
		geocodePosition(miMarcador.getPosition());
		var request = {
		        origin: glatlon,
		        destination: miMarcador.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap(mapa);
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	
		 google.maps.event.addListener(miMarcador, 'dblclick', function(){ 
		openInfoWindow(miMarcador); 
		closeInf(miMarcador);
		rutas(miMarcador);updateMarkerPosition(miMarcador.getPosition());
		geocodePosition(miMarcador.getPosition());
		var request = {
		        origin: glatlon,
		        destination: miMarcador.getPosition(),
		        travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.DirectionsUnitSystem.METRIC ,
		        provideRouteAlternatives: true
	    };
	directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap(mapa);
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("There is no directions available between these two points");
	        }
	    });
		
	});
	
		 
         }   
      }
   });
    });
     
    function actualizarCampos(objDireccion){
        $('#latitud').val(objDireccion.geometry.location.lat());
        $('#longitud').val(objDireccion.geometry.location.lng());
        $('#direccion').val(objDireccion.formatted_address);
    }
 
	  
    }
 
    function onError_HighAccuracy(error) {// dispositivo sin GPS
      if (error.code == error.TIMEOUT){ // si no ha dado tiempo volvemos a intentarlo
        navigator.geolocation.getCurrentPosition(
          onSuccess_HighAccuracy,
          onError_LowAccuracy, // si ha dado error desistimos
          {
             maximumAge: 30000, timeout: 27000,
            enableHighAccuracy: false
          }
        );
      }
      else{// mostramos una alerta con el error
        alert(JSON.stringify(error)); 
      }
    }
 
    function onError_LowAccuracy(error) { // mostramos una alerta con el error
      alert(JSON.stringify(error)); 
    }
    $scope.mapa = mapa;
  }
 
  // añadimos el evento al mapa para iniciar el proceso
  google.maps.event.addDomListener(window, 'load', iniciarMapa);
 // fin e inicio de b_dir
 
 $scope.saveData = function(v){
		window.localStorage.setItem("data", v);
	}
	$scope.loadData = function(){
		alert(window.localStorage.getItem("data"));
		
	}
   
});

function locals(){
			window.localStorage['favoritos'] = angular.toJson(favoritos);
			
			//window.localStorage.setItem("favorito", v);
		
		}
var favoritos = angular.fromJson(window.localStorage['favoritos'] || '[]');

function getNote(favoritoId){
		for (var i = 0; i < favoritos.length; i++){
			if(favoritos[i].id === favoritoId){
				return favoritos[i];
			}
		}
		return undefined;
}   
function crearFavorito(favorito){
		favoritos.push(favorito);
}
	
app.controller('ListCtrl', function($scope){
	
	$scope.favoritos = favoritos;
	
});

app.controller('AddCtrl', function($scope, $state){
	
	$scope.favorito = {
		id: new Date().getTime().toString(),
		nombre: '',
		descripcion:''
		
	};
	
	$scope.save = function(){
		crearFavorito($scope.favorito);
		$state.go('index');
		locals();
	}
});



app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('index',{
		url:'/',
		templateUrl:'tpl/index.html',
		controller: 'MapaCtrl'
	});
	$stateProvider.state('list',{
		url:'/list',
		templateUrl:'tpl/list.html'
	});
	$stateProvider.state('edit',{
		url:'/edit',
		templateUrl:'tpl/edit.html'
	});
	$stateProvider.state('add',{
		url:'/add',
		templateUrl:'tpl/edit.html',
		controller: 'AddCtrl'
	});
	
	$urlRouterProvider.otherwise('/');
});

//var notes = angular.fromJson(window.localStorage['notes'] || '[]');

app.controller("favorito", function($scope){
	
	$scope.saveData = function(v){
		window.localStorage.setItem("data", v);
	}
	$scope.loadData = function(){
		alert(window.localStorage.getItem("data"));
	}
	
});







}());



